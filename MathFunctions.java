//Copyright (C) 2021 Ethan Masse
//This program is free software: you can redistribute it and/or modify it under the terms of the
//GNU Affero General Public License as published by the Free Software Foundation, version 3.
//
//This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
//without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program.
//If not, see <https://www.gnu.org/licenses/>

class MathFunctions
{
        //Euclid's method for greatest common divisor.
        //This implementation based on a pseudocode implementation
        //on Wikipedia (Euclidean algorithms page).
        public static int gcd(int inputOne, int inputTwo)
        {
		inputOne = Math.abs(inputOne);
		inputTwo = Math.abs(inputTwo);

                int temp;

		//Arbitrarily pick that gcd(0, 0) equals 1,
		//to eliminate the possibility of dividing
		//by 0.
		if (inputOne == 0 && inputTwo == 0)
		{
			return 1;
		}

                while (inputTwo != 0)
                {
                        temp = inputTwo;
                        inputTwo = inputOne % inputTwo;
                        inputOne = temp;
                }

                return inputOne;
        }

        public static long gcd(long inputOne, long inputTwo)
        {
		inputOne = Math.abs(inputOne);
		inputTwo = Math.abs(inputTwo);

                long temp;

		//Arbitrarily pick that gcd(0, 0) equals 1,
		//to eliminate the possibility of dividing
		//by 0.
		if (inputOne == 0 && inputTwo == 0)
		{
			return 1;
		}

                while (inputTwo != 0)
                {
                        temp = inputTwo;
                        inputTwo = inputOne % inputTwo;
                        inputOne = temp;
                }
                return inputOne;
        }

	public static long exp(long inputOne, long inputTwo)
	{
		if (inputOne == 0 && inputTwo == 0)
		{
			System.out.println("Error: exp raising 0 to the 0th power is undefined!");
			return -1;
		}

		if (inputOne == 0)
		{
			return 0;
		}

		if (inputTwo == 0)
		{
			return 1;
		}

		if (inputTwo < 0)
		{
			System.out.println("Error: exp negative exponents not supported!");
			return -1;
		}

		long result = 1;

		for (int iii = 0; iii < inputTwo; ++iii)
		{
			result *= inputOne;
		}

		return result;
	}
}

