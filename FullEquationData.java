//Copyright (C) 2021 Ethan Masse
//This program is free software: you can redistribute it and/or modify it under the terms of the
//GNU Affero General Public License as published by the Free Software Foundation, version 3.
//
//This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
//without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program.
//If not, see <https://www.gnu.org/licenses/>

//This class creates objects which are used to store the full equation that is being solved.
//EquationData objects can be either variables (with or without coefficients), or operators such as
//+, -, *, /. So, by creating an array of EquationData objects, we can hold the raw data
//from an entire equation.
//
//For example, 10x^5y^5 / 20x^4y^3z^3 would be stored as three objects:
//10x^5y^5,
//the division symbol /,
//and 20x^4y^3z^3.
class FullEquationData
{
	private int length = 0;
	private EquationData[] data = null;
	private boolean complete = false;

	//If someone tries to use a generic constructor instead of this one (or puts in wrong input),
	//length should be zero, so just checking for length == 0 would be a good check for an
	//invalid FullEquationData object.
	public FullEquationData(int size)
	{
		if (size < 0)
		{
			this.length = 0;
		}

		else
		{
			this.length = size;
		}

		this.data = new EquationData[this.length];
	}

	public FullEquationData copy()
	{
		FullEquationData result = new FullEquationData(this.length);

		for (int iii = 0; iii < result.getSize(); ++iii)
		{
			result.setEquationDataAt(this.data[iii], iii);
		}

		result.setCompletionStatus(this.complete);

		return result;
	}

	//Getters and setters.
	public boolean isComplete()
	{
		return this.complete;
	}

	public void setCompletionStatus(boolean input)
	{
		this.complete = input;
	}

	public int getSize()
	{
		return this.length;
	}

	public EquationData getEquationDataAt(int index)
	{
		if (0 <= index && index < this.length)
		{
			return this.data[index];
		}

		System.out.println("Error: getEquationDataAt tried to get equation data outside of bounds!");
		return null;
	}

	public void setEquationDataAt(EquationData input, int index)
	{
		if (0 <= index && index < this.length)
		{
			this.data[index] = input;
		}

		else
		{
			System.out.println("Error: setEquationDataAt tried to set equation data outside of bounds!");
		}
	}
}
