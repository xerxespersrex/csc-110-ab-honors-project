//Copyright (C) 2021 Ethan Masse
//This program is free software: you can redistribute it and/or modify it under the terms of the
//GNU Affero General Public License as published by the Free Software Foundation, version 3.
//
//This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
//without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program.
//If not, see <https://www.gnu.org/licenses/>


//Ethan Masse's CSC 110 AB Honors Project
//a simple equation simplifier

/**
 * The main class for the HonorsProject program. The program is the accompanying applied part
 * of my (Ethan Masse's) honors project for the class CSC 110 AB (Introduction to Computer
 * Science - Java). This class is where the main() function for the entire program is held,
 * as well as the basic skeleton of how the program operates. Most details and algorithms
 * are not stored here, and are instead stored inside of the EquationMethods class.
 *
 * The program is intended to simplify expressions containing multiple variables, and supports
 * addition, subtraction, multiplication, and division (but not parentheses). It also outputs
 * all results in canonical form (that is, two inputs which give an equivalent output will
 * always give the output exactly the same, due to some tricks and the specifics of the
 * simplicity of the program's scope). In essence: the program takes in a command line argument
 * (the first argument) which it reads as the equation to be simplified. It breaks down the
 * String argument into EquationData objects which represent either a term in the equation or
 * an operator (+, -, *, /). Then, these EquationData objects are stored in an array
 * inside of a FullEquationData object, which makes up the internal representation of the
 * entire equation. The program steps through and completes one operation in the equation at a
 * time, following the order of operations, until no more operations can be performed - at
 * which point, the FullEquationData object which is left is sorted to canonical form and then
 * printed.)
 *
 * VERBOSE_FLAG: if this is set to true (which is default), the program will print verbose
 * output of each step of the calculation. If this is set to false, the equation will only
 * print the final resulting equation (which is useful for piping on the command line).
 *
 * @author Ethan Masse
 */
public class HonorsProject
{
	/**
	 * The main function for the HonorsProject program. Calling this function starts the
	 * program. It needs a command line argument to work (only one), which is the
	 * equation which the program will simplify. Any arguments after the first are
	 * ignored.
	 *
	 * @param args The command line arguments for the program. The first argument
	 * 	       (args[0]) is read in as the equation which the program
	 * 	       simplifies.
	 */
	public static void main(String[] args)
	{
		//Number of attempts at simplifying the input equation before exiting.
		final int MAX_STEPS = 500;

		//If true, output each step of the equation instead of just the result.
		final boolean VERBOSE_FLAG = true;

		//TODO: handle command line options and make this smarter.
		String equation;

		if (args.length != 0)
		{
			equation = args[0];
		}

		else
		{
			System.out.println("Error: main need an equation string as an input argument to continue!");
			return;
		}

		//Parse input string equation into internal FullEquationData object.
		FullEquationData fullEquationData = EquationMethods.parseEquationString(equation);

		//If the equation is null, fail.
		if (fullEquationData == null)
		{
			System.out.println("Error: main parsing equation \"" + equation + "\" failed!");
			return;
		}

		//Main program loop: does a "step" (aka a single operation) from the input equation repeatedly
		//until no more operations can be performed, at which point the equation is labelled "complete"
		//internally, and the loop edits.
		int iii;

		for (iii = 0; ((iii < MAX_STEPS) && !(fullEquationData.isComplete())); ++iii)
		{
			if (VERBOSE_FLAG)
			{
				EquationMethods.printlnFullEquationData(fullEquationData);
			}

			fullEquationData = EquationMethods.equationStep(fullEquationData);

			if (fullEquationData == null)
			{
				System.out.println("Error: main fullEquationData became null at step " + iii + "!");
				return;
			}

			fullEquationData = EquationMethods.checkFullEquationData(fullEquationData);
		}

		//If we exceed the maximum amount of steps for the equation, fail.
		if (iii >= MAX_STEPS)
		{
			System.out.println("Error: main max amount of steps exceeded!");
			return;
		}

		//Canonical form: sort the resulting equation alphabetically before outputting.
		if (VERBOSE_FLAG)
		{
			System.out.println("Sorting alphabetically.");
		}

		fullEquationData = EquationMethods.sortCompletedEquation(fullEquationData);

		if (VERBOSE_FLAG)
		{
			System.out.print("Final result: ");
		}

		//Print final result.
		EquationMethods.printlnFullEquationData(fullEquationData);
	}
}

