//Copyright (C) 2021 Ethan Masse
//This program is free software: you can redistribute it and/or modify it under the terms of the
//GNU Affero General Public License as published by the Free Software Foundation, version 3.
//
//This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
//without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program.
//If not, see <https://www.gnu.org/licenses/>

/**
 * Contains general utility methods that aren't math utility methods or methods specific
 * to this program.
 *
 * @author Ethan Masse
 */
class UtilityMethods
{
	/**
	 * Converts an array of characters which are digits into a long. Probably should
	 * be replaced by the internal Long.parseLong() function, but for the moment
	 * this works just fine.
	 *
	 * @param array The character array being turned into a long.
	 * @param length The length of the array.
	 * @return A long which is the result of the conversion. The way this program is
	 * 	   written, it can only convert positive numbers, so any negative that
	 * 	   results is an error. Returning -1 means that the length argument is
	 * 	   larger than the actual size of the array. -2 means the array is too
	 * 	   long. -3 means that there are characters other than the digits 0-9
	 * 	   within the array.
	 */
	public static long convertCharArrayToLong(char[] array, int length)
	{
		//If the input length argument is larger than the array, fail.
		if (length > array.length)
		{
			System.out.println("Error: convertCharArrayToLong length argument is larger than the array's size!");
			return -1;
		}

		//Max length in digits of the input number.
		final int MAX_LENGTH = 12;

		//If input array is too long, fail.
		if (length > MAX_LENGTH)
		{
			System.out.println("Error: convertCharArrayToLong array is too long!");
			return -2;
		}

		//If any characters in the array are not strictly numbers,
		//fail.
		for (int iii = 0; iii < length; ++iii)
		{
			if (!(Character.isDigit(array[iii])))
			{
				System.out.println("Error: convertCharArrayToLong array is not only numbers!");
				return -3;
			}
		}

		//Long to store result.
		long result = 0;

		//Loop through each digit, multiply the value of that character
		//converted to an integer by the proper power of 10
		//for that place in the number, and t
		for (int iii = 0; iii < length; ++iii)
		{
			//TODO: I don't really understand Character.digit, but I believe
			//that this should convert the char to a number. The second parameter
			//10 specifies that the number is in base 10.
			result += MathFunctions.exp((long)10, (long)(length - iii - 1)) * (long)(Character.digit(array[iii], 10));
		}

		return result;
	}

	/**
	 * Checks if the input char is an English letter. I'm not sure if the built-in function
	 * to see if a char is a letter counts foreign letters, so I created my own which only
	 * includes a-z and A-Z.
	 *
	 * @param input The character to be checked.
	 * @return True if the input letter is an english letter from a-z or A-Z. False otherwise.
	 */
	public static boolean isEnglishLetter(char input)
	{
		if ((input >= 'a' && input <= 'z') || (input >= 'A' && input <= 'Z'))
		{
			return true;
		}

		return false;
	}

	/**
	 * Compares two letters alphabetically to see which comes first. If two of the letters
	 * are the same but one is capitalized, then the one that is capitalized is said to
	 * come before the one that is lowercase alphabetically. Otherwise it works as you
	 * would expect. The order of parameters is obviously important, because we have
	 * to indicate which of the two comes first with our return value.
	 *
	 * @param inputOne The first char to be compared.
	 * @param inputTwo The second char to be compared.
	 * @return 1 if inputOne comes first alphabetically; 2 if inputTwo comes first; 3 if
	 *         they are the exact same letter (including capitalization); -1 if there
	 *         is an error.
	 */
	public static int compareLettersAlphabetically(char inputOne, char inputTwo)
	{
		final int FIRST_COMES_FIRST = 1;
		final int SECOND_COMES_FIRST = 2;
		final int SAME = 3;
		final int ERROR = -1;

		//If either input is not a Roman letter, fail.
		if (!(isEnglishLetter(inputOne)))
		{
			System.out.println("Error: compareLettersAlphabetically inputOne is not a letter!");
			return ERROR;
		}

		if (!(isEnglishLetter(inputTwo)))
		{
			System.out.println("Error: compareLettersAlphabetically inputTwo is not a letter!");
			return ERROR;
		}

		//If both characters are the same, return SAME (3).
		if (inputOne == inputTwo)
		{
			return SAME;
		}

		final boolean INPUT_ONE_IS_UPPERCASE;
		final boolean INPUT_TWO_IS_UPPERCASE;

		//Mark whether each one is uppercase or not.
		//If so, convert to the value of the lowercase letter
		//so that we can compare values.
		if (Character.isUpperCase(inputOne))
		{
			INPUT_ONE_IS_UPPERCASE = true;
			inputOne = Character.toLowerCase(inputOne);
		}

		else
		{
			INPUT_ONE_IS_UPPERCASE = false;
		}

		if (Character.isUpperCase(inputTwo))
		{
			INPUT_TWO_IS_UPPERCASE = true;
			inputTwo = Character.toLowerCase(inputTwo);
		}

		else
		{
			INPUT_TWO_IS_UPPERCASE = false;
		}

		//If the two are the same, put whichever is uppercase first.
		//This check is very simple but should work.
		if (inputOne == inputTwo)
		{
			if (INPUT_ONE_IS_UPPERCASE)
			{
				if (INPUT_TWO_IS_UPPERCASE)
				{
					return SAME;
				}

				return FIRST_COMES_FIRST;
			}

			if (INPUT_TWO_IS_UPPERCASE)
			{
				return SECOND_COMES_FIRST;
			}

			return SAME;
		}

		//If they aren't the same letter, whichever comes first
		//alphabetically goes first.
		if (inputOne < inputTwo)
		{
			return FIRST_COMES_FIRST;
		}

		return SECOND_COMES_FIRST;
	}

}
