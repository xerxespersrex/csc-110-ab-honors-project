//Copyright (C) 2021 Ethan Masse
//This program is free software: you can redistribute it and/or modify it under the terms of the
//GNU Affero General Public License as published by the Free Software Foundation, version 3.
//
//This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
//without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program.
//If not, see <https://www.gnu.org/licenses/>

/**
 * An EquationData object holds the data of an individual element of the equation
 * being simplified. It can either hold a term (such as 4, or 7j, or 20j^2y^3, etc.),
 * or it can hold an operator (+, -, *, /). The method isOperator() determines which
 * of the two the object is.
 *
 * These objects are joined together in a array within the FullEquationData class to
 * create the internal representation of the entire equation. For example, the
 * EquationData objects representing 2, +, and 2 can be strung together to get the
 * equation 2+2. The program then can manipulate this equation, and arrive at the
 * simplified answer, 4.
 *
 * @author Ethan Masse
 */
class EquationData
{
	//Determines whether operator or number/variable.
	private boolean isOperator;

	//Character of operator, if operator.
	private char operatorChar;

	//For numbers/variables, stores coefficient
	//numerator and denominator.
	private long numerator = 0;
	private long denominator = 1;

	//Stores variable parts and their exponents.
	private int size = 0;
	private char[] vars = null;
	private long[] varExps = null;

	/**
	 * Constructor used for terms (not operators). The input determines how
	 * many variables then term contains.
	 *
	 * @param variableCount Sets the amount of variables the term contains,
	 * 			which cannot be changed.
	 */
	public EquationData(int variableCount)
	{
		this.isOperator = false;

		if (variableCount < 0)
		{
			this.size = 0;
		}

		else
		{
			this.size = variableCount;
		}

		this.vars = new char[this.size];
		this.varExps = new long[this.size];
	}

	/**
	 * Constructor used for operators (not terms). The input determines which
	 * operator the EquationData object represents.
	 *
	 * @param op The character representing the operator the EquationData object
	 * 	     is meant to represent.
	 */
	public EquationData(char op)
	{
		this.isOperator = true;

		if (op != '*' && op != '/' && op != '+' && op != '-')
		{
			System.out.println("Error: EquationData invalid operator!");
		}

		this.operatorChar = op;
	}

	/**
	 * Copies this EquationData object, and returns a new one that is exactly
	 * the same. For safety, copies are used constantly throughout the program
	 * instead of modifying objects directly to avoid any side effect bugs
	 * entirely.
	 *
	 * @return An EquationData object which is exactly the same as this.
	 */
	public EquationData copy()
	{
		EquationData result;

		if (this.isOperator)
		{
			result = new EquationData(this.operatorChar);
		}

		else
		{
			result = new EquationData(this.size);

			for (int iii = 0; iii < result.getSize(); ++iii)
			{
				result.setVariableAt(this.vars[iii], iii);
				result.setVariableExponentAt(this.varExps[iii], iii);
			}

			result.setNumerator(this.numerator);
			result.setDenominator(this.denominator);
		}

		return result;
	}

	//======================Getters and setters==========================
	/**
	 * Returns whether the object is an operator or a term.
	 *
	 * @return True if an operator, false otherwise.
	 */
	public boolean isOperator()
	{
		return this.isOperator;
	}

	/**
	 * Returns the character representing the operator, if the object is an operator -
	 * otherwise prints an error and returns a space.
	 *
	 * @return The character that represents the operator, or a space if the object is
	 * 	   not an operator.
	 */
	public char getOperator()
	{
		if (!(this.isOperator))
		{
			System.out.println("Error: getOperator this object is not an operator!");
			return ' ';
		}

		return this.operatorChar;
	}

	/**
	 * Returns the numerator of the coefficient of the term, if the object is a term -
	 * otherwise prints an error and returns 0.
	 *
	 * @return The numerator of the coefficient as a long.
	 */
	public long getNumerator()
	{
		if (this.isOperator)
		{
			System.out.println("Error: getNumerator this object is an operator!");
			return 0;
		}

		return this.numerator;
	}

	/**
	 * Sets the numerator of the coefficient of the term to the input long, if the object
	 * is a term - otherwise prints an error and does nothing.
	 *
	 * @param input The new value the numerator of this object will be set to.
	 */
	public void setNumerator(long input)
	{
		if (this.isOperator)
		{
			System.out.println("Error: setNumerator this object is an operator!");
			return;
		}

		this.numerator = input;
	}

	/**
	 * Gets the denominator of the coefficient of the term as a long, if the object is a
	 * term - otherwise prints an error and returns 0.
	 *
	 * @return The denominator of the coefficient as a long.
	 */
	public long getDenominator()
	{
		if (this.isOperator)
		{
			System.out.println("Error: getDenominator this object is an operator!");
			return 0;
		}

		return this.denominator;
	}

	/**
	 * Sets the denominator of the coefficient of the term to the input long, if the
	 * object is a term - otherwise prints an error and does nothing.
	 *
	 * @param input The new value the denominator of this object will be set to.
	 */
	public void setDenominator(long input)
	{
		if (this.isOperator)
		{
			System.out.println("Error: setDenominator this object is an operator!");
			return;
		}

		this.denominator = input;
	}

	/**
	 * Gets the amount of variables in the object.
	 *
	 * Note that this works using a "perfect length" mentality, where the size is just
	 * the maximum amount of variables. It is possible to use EquationData objects as
	 * imperfect sized arrays during some calculations (as is done within the program
	 * still in a few places), but this is bad practice and by the end of any method
	 * you eventually have to return a perfect sized EquationData object again anyways.
	 *
	 * @return The maximum amount of variables the object can hold as an int.
	 */
	public int getSize()
	{
		if (this.isOperator)
		{
			System.out.println("Error: getSize this object is an operator!");
			return 0;
		}

		return this.size;
	}

	/**
	 * Returns the character of the variable at the given index.
	 *
	 * The exponent can also be retrieved by using the separate getVariableExponentAt()
	 * method.
	 *
	 * @param index The index of the variable we want to get the letter ("name") of.
	 * @return The character of the variable, or a space if there is an error.
	 */
	public char getVariableAt(int index)
	{
		if (this.isOperator)
		{
			System.out.println("Error: setVariableAt this object is an operator!");
			return ' ';
		}

		if (index < 0 || index >= this.size)
		{
			System.out.println("Error: setVariableAt tried to set variable outside of bounds!");
			return ' ';
		}

		return this.vars[index];
	}

	/**
	 * Sets the character of the variable at the given index to the given
	 * input character. If the input index is outside of bounds of the
	 * object, print an error message and do nothing.
	 *
	 * The exponent can also be set using the setVariableExponentAt() method.
	 *
	 * @param input The char which will replace the old variable char.
	 * @param index The index of the variable we are replacing.
	 */
	public void setVariableAt(char input, int index)
	{
		if (this.isOperator)
		{
			System.out.println("Error: setVariableAt this object is an operator!");
			return;
		}

		if (index < 0 || index >= size)
		{
			System.out.println("Error: setVariableAt tried to set variable outside of bounds!");
			return;
		}

		this.vars[index] = input;
	}

	/**
	 * Returns the exponent of the variable at the given index.
	 *
	 * The character of the variable can also be retrieved by using the separate
	 * getVariableAt() method.
	 *
	 * @param index The index of the variable we want to get the exponent of.
	 * @return The exponent of the variable as a long, or 0 if there was an error..
	 */
	public long getVariableExponentAt(int index)
	{
		if (this.isOperator)
		{
			System.out.println("Error: getVariableExponentAt this object is an operator!");
			return 0;
		}

		if (index < 0 || index >= this.size)
		{
			System.out.println("Error: getVariableExponentAt tried to set variable outside of bounds!");
			return 0;
		}

		return this.varExps[index];
	}

	/**
	 * Sets the exponent of the variable at the given index to the given
	 * input long. If the input index is outside of bounds of the
	 * object, print an error message and do nothing.
	 *
	 * The character can also be set using the setVariableAt() method.
	 *
	 * @param input The long which will replace the old exponent long.
	 * @param index The index of the variable exponent we are replacing.
	 */
	public void setVariableExponentAt(long input, int index)
	{
		if (this.isOperator)
		{
			System.out.println("Error: setVariableExponentAt this object is an operator!");
			return;
		}

		if (index < 0 || index >= this.size)
		{
			System.out.println("Error: setVariableExponentAt tried to set variable outside of bounds!");
			return;
		}

		this.varExps[index] = input;
	}
}
