//Copyright (C) 2021 Ethan Masse
//This program is free software: you can redistribute it and/or modify it under the terms of the
//GNU Affero General Public License as published by the Free Software Foundation, version 3.
//
//This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
//without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//See the GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License along with this program.
//If not, see <https://www.gnu.org/licenses/>

/**
 * The EquationMethods class defines the static methods which make up 99% of the program.
 * In essence, the program uses a functional programming paradigm, with most functions
 * being stored within this class. However, the added benefits of object-oriented paradigm
 * are taken advantage of for the classes which hold the internal representation of the
 * equation being operated upon (which are the classes EquationData and FullEquationData).
 *
 * @author Ethan Masse
 */
class EquationMethods
{
	/**
	 * A helper function for parseEquationString. The parseEquationString function
	 * steps through the equation and breaks the equation down into char[] arrays
	 * for the terms, which are then passed to this function to parse into an
	 * EquationData object.
	 *
	 * @param inputArray The character array which stores the term to be parsed
	 * 		     into an EquationData object.
	 * @param inputLength The length of the input character array.
	 * @return Returns the EquationData object parsed on a successful parse.
	 * 	   If there are any errors during the parsing, returns null.
	 * @see parseEquationString
	 */
	public static EquationData parseSingleData(char[] inputArray, int inputLength)
	{
		//Maximum size constants.
		final int MAX_VARIABLES = 250;
		final int MAX_NUMBER_ARRAY = 12;

		//Stores all the pieces of the data we're going to return.
		long tempNum = 0;
		long tempDenom = 1;
		char[] tempVariables = new char[MAX_VARIABLES];
		long[] tempExponents = new long[MAX_VARIABLES];

		//Counts where we are in our tempVariables and tempExponents arrays.
		//(Also the size.)
		int tempCounter = 0;

		//Stores the numbers as arrays of characters which we will then convert
		//back to a long.
		char[] numberArray = new char[MAX_NUMBER_ARRAY];

		//Stores where we are in the number array.
		int numberCounter = 0;

		//Our indexing loop variable. (Our algorithm later will use it in a way
		//in which we cannot simply declare it in for loops like usual.)
		int iii = 0;

		//The resulting object we will return.
		EquationData result = null;

		if (inputLength <= 0)
		{
			System.out.println("Error: parseSingleData inputLength is <= 0!");
			return null;
		}

		//We shouldn't hit this, but just in case we just get an operator,
		//return an operator EquationData object.
		if ((inputLength == 1) && ((inputArray[0] == '+') || (inputArray[0] == '-') || (inputArray[0] == '*') || (inputArray[0] == '/')))
		{
			System.out.println("Warning: parseSingleData inputArray is length 1 and only an operator!");

			result = new EquationData(inputArray[0]);
			return result;
		}

		//Check to make sure we don't have any operators even if inputLength != 1.
		//If we do, that means inherently that we aren't dealing with a
		//single EquationData object, and fail.
		for (iii = 0; iii < inputLength; ++iii)
		{
			if ((inputArray[iii] == '+') || (inputArray[iii] == '-') || (inputArray[iii] == '*') || (inputArray[iii] == '/'))
			{
				System.out.println("Error: parseSingleData inputArray is not length 1 and includes operators!");
				return null;
			}
		}

		//In general, a data object can only contain letters a-z, A-Z, numbers 0-9,
		//and the carrot symbol (^). If it contains anything else, fail.
		for (iii = 0; iii < inputLength; ++iii)
		{
			if (Character.isLetterOrDigit(inputArray[iii]) || (inputArray[iii] == '^'))
			{
				//Do nothing.
			}
			else
			{
				System.out.println("Error: parseSingleData inputArray is not only alphanumeric characters or \'^\'!");
				return null;
			}
		}

		//Prepare our index variables. (We will be using this same iii
		//throughout the parsing without resetting it to 0.)
		iii = 0;

		numberCounter = 0;

		//If the term doesn't start with a digit, then just set the
		//numerator and denominator of the coefficient to 1.
		if (!(Character.isDigit(inputArray[iii])))
		{
			tempNum = 1;
			tempDenom = 1;
		}

		//Otherwise...
		else
		{
			//Loop through the equation string until we reach something that's
			//not a number, or until we are parsing too many numbers in a row
			//or exceeded the length of the string.
			while ((iii < inputLength) && (numberCounter < MAX_NUMBER_ARRAY) && Character.isDigit(inputArray[iii]))
			{
				//Save the numbers in the string to an array.
				numberArray[numberCounter] = inputArray[iii];

				++numberCounter;

				++iii;
			}

			//If there were too many numbers, fail.
			if (numberCounter >= MAX_NUMBER_ARRAY)
			{
				System.out.println("Error: parseSingleData number being parsed had too many digits!");

				return null;
			}

			//Convert the number array and its length into a long, and
			//save it as the numerator of the coefficient.
			tempNum = UtilityMethods.convertCharArrayToLong(numberArray, numberCounter);
			tempDenom = 1;

			//Reset our number array for later use.
			numberArray = new char[MAX_NUMBER_ARRAY];
			numberCounter = 0;
		}

		//If after we parse the number we're at the end of the string,
		//return an object of size 0 with the recorded coefficient.
		if (iii >= inputLength)
		{
			result = new EquationData(0);
			result.setNumerator(tempNum);
			result.setDenominator(tempDenom);

			return result;
		}

		//Otherwise, if we have more to the string, start looping
		//as long as we're pointing at a letter.
		while (iii < inputLength && Character.isLetter(inputArray[iii]))
		{
			//If we have too many variables recorded in our object, fail.
			if (tempCounter >= MAX_VARIABLES)
			{
				System.out.println("Error: parseSingleData too many variables in parsed term!");
				return null;
			}

			//Record the letter as a variable and increment iii.
			tempVariables[tempCounter] = inputArray[iii];

			++iii;

			//If there's a carrot ^ symbol, that means we have
			//an exponent...
			if (iii < inputLength && (inputArray[iii] == '^'))
			{
				//Increment iii to skip over the carrot ^ symbol.
				++iii;

				//If the next thing is not a digit, fail.
				if (!(Character.isDigit(inputArray[iii])))
				{
					System.out.println("Error: parseSingleData improper use of ^ in input!");

					return null;
				}

				//Otherwise, if it is a digit, use the same
				//loop construction we used for the
				//coefficient to record the exponent into
				//a char array.
				while ((iii < inputLength) && (numberCounter < MAX_NUMBER_ARRAY) && Character.isDigit(inputArray[iii]))
				{
					numberArray[numberCounter] = inputArray[iii];

					++numberCounter;

					++iii;
				}

				//Convert our char array to a long, and save it
				//as the exponent.
				tempExponents[tempCounter] = UtilityMethods.convertCharArrayToLong(numberArray, numberCounter);

				//Reset our char[] number array.
				numberCounter = 0;

				numberArray = new char[MAX_NUMBER_ARRAY];
			}

			//If there wasn't a carrot next, just save the exponent as 1.
			else
			{
				tempExponents[tempCounter] = 1;
			}

			//iii is incremented throughout the algorithm, so the only thing
			//we need to increment here is our counter for the number of
			//variables (which we also use as an index variable for the
			//tempVariables[] and tempExponents[] arrays.
			++tempCounter;
		}

		//If we failed anywhere along the way of this algorithm and did not end
		//up with the expected next character, that means that we will end up
		//with iii not having reached the end of the input term char array.
		//If this happens, fail.
		if (iii < inputLength)
		{
			System.out.println("Error: parsing failed before reaching the end of the term!");

			return null;
		}

		//Create the resulting EquationData object we will return.
		result = new EquationData(tempCounter);

		//Copy over the contents of tempVariables[] and tempExponents[].
		for (iii = 0; iii < tempCounter; ++iii)
		{
			result.setVariableAt(tempVariables[iii], iii);
			result.setVariableExponentAt(tempExponents[iii], iii);
		}

		//Set the numerator and denominator of the coefficient to
		//tempNum and tempDenom.
		result.setNumerator(tempNum);
		result.setDenominator(tempDenom);

		//Return.
		return result;
	}

	/**
	 * Converts the user input string into a FullEquationData object. The
	 * program first reads in the user's equation to be simplified as a command
	 * line argument, which is a string. This function breaks down that string
	 * into individual EquationData objects, parsed by parseSingleData(),
	 * and then combines them together into one complete FullEquationData
	 * object.
	 *
	 * @param input The user entered string to convert to a FullEquationData
	 * 	        object representation.
	 * @return Returns a FullEquationData representation of the equation in
	 * 	   the input string.
	 * @see parseSingleData
	 */
	public static FullEquationData parseEquationString(String input)
	{
		//Hardcode a maximum amount of EquationData objects to create from
		//the string.
		final int MAX_DATA = 500;

		//Hardcode the maximum amount of characters in a single
		//EquationData string.
		final int MAX_DATA_SIZE = 500;

		//Stores the current data object we're creating as an
		//array to be processed by parseSingleData.
		char[] singleArray = new char[MAX_DATA_SIZE];

		//Stores the length of singleArray for parseSingleData.
		int singleLength = 0;

		//Stores the resulting FullEquaitonData as we go along.
		EquationData[] tempData = new EquationData[MAX_DATA];

		//Counts where we are inside of the FullEquationData object.
		int resultCount = 0;

		//Flag for whether or not we're currently working on an EquationData
		//object.
		boolean insideObject = false;

		for (int iii = 0; iii < input.length(); ++iii)
		{
			//If we're pointing at a space, do nothing.
			if (Character.isWhitespace(input.charAt(iii)))
			{
				//Do nothing.
			}

			//If we're pointing at an operator...
			else if ((input.charAt(iii) == '+') || (input.charAt(iii) == '-') || (input.charAt(iii) == '*') || (input.charAt(iii) == '/'))
			{
				//If we reached the end of a term and are now pointing at an operator, parse the term into an object
				//and save it into our tempData array.
				if (insideObject)
				{
					if (resultCount >= MAX_DATA)
					{
						System.out.println("Error: parseEquationString FullEquationData object is too big!");
						return null;
					}

					tempData[resultCount] = parseSingleData(singleArray, singleLength);

					if (tempData[resultCount] == null)
					{
						System.out.println("Error: parseEquationString failed to create data object for tempData at " + resultCount + "!");
						return null;
					}

					++resultCount;

					insideObject = false;

					singleLength = 0;

					singleArray = new char[MAX_DATA_SIZE];
				}

				if (resultCount >= MAX_DATA)
				{
					System.out.println("Error: parseEquationString FullEquationData object is too big!");
					return null;
				}

				//Make an object for the operator we're point at as well.
				tempData[resultCount] = new EquationData(input.charAt(iii));

				++resultCount;
			}

			//If we're not pointing at an operator, then we're reading in a term that we will parse once we're done
			//reading it.
			else
			{
				insideObject = true;

				if (singleLength >= MAX_DATA_SIZE)
				{
					System.out.println("Error: parseEquationString singleArray is too big!");
					return null;
				}

				singleArray[singleLength] = input.charAt(iii);

				++singleLength;
			}
		}

		//At the end, if we didn't end with an operator (that would be invalid input anyways),
		//parse the term we were reading and add that to the end of the tempData array.
		if (insideObject)
		{
			if (resultCount >= (MAX_DATA))
			{
				System.out.println("Error: parseEquationString FullEquationData object is too big!");
				return null;
			}

			tempData[resultCount] = parseSingleData(singleArray, singleLength);

			if (tempData[resultCount] == null)
			{
				System.out.println("Error: parseEquationString failed to create data object for tempData at " + resultCount + "!");
				return null;
			}

			++resultCount;

			insideObject = false;

			singleLength = 0;

			singleArray = new char[MAX_DATA_SIZE];
		}

		//Create a new FullEquationData object with the correct size
		//and copy over the contents of tempData to that resulting object,
		//which we then return.
		FullEquationData result = new FullEquationData(resultCount);

		for (int iii = 0; iii < result.getSize(); ++iii)
		{
			result.setEquationDataAt(tempData[iii].copy(), iii);
		}

		//Check it right away for good measure.
		result = checkFullEquationData(result);

		//If the check failed, print a message that it failed.
		if (result == null)
		{
			System.out.println("Error: parseEquationString resulting object came out null (most likely check failed or input was bad)!");
		}

		return result;
	}

	/**
	 * Performs a single operation in the input equation (by order of
	 * operations), and then returns the resulting equation. If no
	 * operations can be done, the equation is returned with the
	 * completion status set to true, so that the main function can
	 * tell when the equation is at its most simplified.
	 *
	 * @param input The equation so far that will have one of its
	 * 		operations simplified, if possible.
	 * @return Returns a FullEquationData object which is the input
	 * 	   equation with either 0 or 1 of its operators simplified.
	 * 	   If no operation is completed for any reason (most likely
	 * 	   that the equation is already in its most simplified form),
	 * 	   then the completion status of the FullEquationData object
	 * 	   is set to true in order to flag the calculation as being
	 * 	   complete.
	 * @see checkFullEquationData
	 */
	public static FullEquationData equationStep(FullEquationData input)
	{
		//Check every step that the equation doesn't have errors.
		FullEquationData checkedInput = checkFullEquationData(input);

		if (checkedInput == null)
		{
			System.out.println("Error: equationStep checkFullEquationData failed!");
			return null;
		}

		//If checkedInput is only 1 EquationData object long, just return it.
		//If it's also an operator, fail.
		if (checkedInput.getSize() == 1)
		{
			if (checkedInput.getEquationDataAt(0).isOperator())
			{
				System.out.println("Error: equationStep equation is only an operator!");
				return null;
			}

			checkedInput.setCompletionStatus(true);

			return checkedInput;
		}

		//Step through equation, recording it to tempData.
		//If we haven't done an operation yet (each step is designed to do only one operation),
		//and the next data in the equation is a * or / operator, complete that operation, set
		//the operationPerformed flag to true, and record the result.
		//Increment i by 2 to skip over the data just multiplied.
		//
		//NOTE: We assume, because of the checks in checkFullEquationData, that the equation
		//does not start with an operator, end with an operator, or contain two operators
		//in a row. Therefore, it will always go in a pattern of:
		//number, operator, number, operator, number, etc.
		//
		//TODO: This assumption breaks down when using parentheses.
		EquationData[] tempData = new EquationData[checkedInput.getSize()];

		boolean operationPerformed = false;

		int count = 0;

		for (int iii = 0; iii < checkedInput.getSize(); ++iii)
		{
			if (operationPerformed == false && (iii < (checkedInput.getSize() - 2)) && checkedInput.getEquationDataAt(iii + 1).isOperator()
			    && (checkedInput.getEquationDataAt(iii + 1).getOperator() == '*' || checkedInput.getEquationDataAt(iii + 1).getOperator() == '/'))
			{
				if (checkedInput.getEquationDataAt(iii + 1).getOperator() == '*')
				{
					tempData[count] = multiplyEquations(checkedInput.getEquationDataAt(iii), checkedInput.getEquationDataAt(iii + 2));

					if (tempData[count] == null)
					{
						System.out.println("Error: equationStep multiplication resulted in null!");

						return null;
					}
				}

				else
				{
					tempData[count] = divideEquations(checkedInput.getEquationDataAt(iii), checkedInput.getEquationDataAt(iii + 2));

					if (tempData[count] == null)
					{
						System.out.println("Error: equationStep division resulted in null!");

						return null;
					}
				}

				iii += 2;

				operationPerformed = true;
			}

			else
			{
				tempData[count] = checkedInput.getEquationDataAt(iii).copy();
			}

			++count;
		}

		//Addition is very different from multiplication. We need to
		//search through the entire equation for any two compatible
		//data objects to add together. We can check all of them because
		//multiplication/division should always be possible or return an
		//error (e.g. division by zero), so all we're left with by this
		//point is an equation with just addition and subtraction.
		//
		//We need temporary variables for the equationdata to store the
		//results (and to be able to multiply the numerators by -1 if necessary
		//for subtractions).
		//
		//TODO: The assumption of the spacing between operators and data breaks
		//down with parentheses.
		EquationData tempSingleOne = null;
		EquationData tempSingleTwo = null;
		EquationData tempSingleResult = null;

		//We use these variables to mark the positions of the two objects that we
		//added together, so we can skip them when writing the final resulting
		//equation.
		int positionOne = -1;
		int positionTwo = -1;

		//Only calculate if we did not already calculate a multiplication/division.
		if (operationPerformed == false)
		{
			//Loop through all of the input equation's leftover objects
			//and attempt to add them. If it fails, keep going until all
			//combinations have been attempted. (The nice thing about
			//this is if there's only unaddable objects left in the
			//equation, we can just return the equation properly
			//marked as complete!)
			for (int iii = 0; iii < checkedInput.getSize(); iii += 2)
			{
				for (int jjj = iii + 2; jjj < checkedInput.getSize(); jjj += 2)
				{
					//If for some reason the equation is malformed, or we aren't pointing
					//at it properly with the for loops, then fail.
					if ((iii != 0 && !(checkedInput.getEquationDataAt(iii - 1).isOperator())) || !(checkedInput.getEquationDataAt(jjj - 1).isOperator()))
					{
						System.out.println("Error: equationStep failure in addition loop logic!");
						return null;
					}

					//Instead of doing subtraction, we just save
					//the object to a temporary object variable
					//and, if it has a minus sign out front,
					//multiply the numerator by -1.
					if (iii != 0 && checkedInput.getEquationDataAt(iii - 1).getOperator() == '-')
					{
						tempSingleOne = checkedInput.getEquationDataAt(iii).copy();
						tempSingleOne.setNumerator(tempSingleOne.getNumerator() * (-1));
					}

					else
					{
						tempSingleOne = checkedInput.getEquationDataAt(iii).copy();
					}

					if (checkedInput.getEquationDataAt(jjj - 1).getOperator() == '-')
					{
						tempSingleTwo = checkedInput.getEquationDataAt(jjj).copy();
						tempSingleTwo.setNumerator(tempSingleTwo.getNumerator() * (-1));
					}

					else
					{
						tempSingleTwo = checkedInput.getEquationDataAt(jjj).copy();
					}

					//If addition succeeded, mark the positions, save the result, and break.
					if ((tempSingleResult = addEquations(tempSingleOne, tempSingleTwo)) != null)
					{
						positionOne = iii;
						positionTwo = jjj;

						operationPerformed = true;
						break;
					}

					tempSingleOne = null;
					tempSingleTwo = null;
				}

				//If addition just succeeded above, break from this loop too. (Probably can be done
				//with two "break;"s right next to each other above, but I'm not sure, and it's probably
				//bad practice anyways.)
				if (operationPerformed == true)
				{
					break;
				}
			}

			//Now we take our addition result, and the positions of the two items we added, and create a new
			//equation to return.
			//
			//TODO: triple check to make sure this logic is correct about how we remove the parts of the equation
			//that we added.
			if (tempSingleResult != null)
			{
				//First, put the result of the addition at the front of the new equation (for simplicity).
				count = 0;

				tempData[count] = tempSingleResult;

				++count;

				//If we put a term at the beginning of the equation, it won't have
				//an operator following it before the old first term of the equation.
				//Since it was the first term, that means that it didn't have a leading
				//operator anywyas, so just put a + inbetween them.
				if (positionOne != 0 && positionTwo != 0)
				{
					tempData[count] = new EquationData('+');

					++count;
				}

				//Skip the objects we added. Otherwise, write the data from our checked input to tempData.
				for (int iii = 0; iii < checkedInput.getSize(); ++iii)
				{
					//If one of our added terms is at the beginning of the
					//equation, we can just skip that one term (since there's no
					//leading terms in equations).
					if (iii == 0 && (iii == positionOne || iii == positionTwo))
					{
						//Do nothing.
					}

					//If we're point at the operator in front of a term we already
					//added, we skip two places ahead (to skip the term's old
					//addition/subtraction sign, and the term itself).
					else if (iii + 1 == positionOne || iii + 1 == positionTwo)
					{
						++iii;
					}

					//Otherwise, if we're not pointing at a term and its operator
					//that we've already added previously, just copy over the
					//EquationData to tempData[].
					else
					{
						tempData[count] = checkedInput.getEquationDataAt(iii).copy();

						++count;
					}
				}
			}
		}


		//Copy the results to a new FullEquationData object that we will return.
		//(This is done so that the length of the resulting object that we return
		//matches exactly with the amount of EquationData objects actually in
		//the array.)
		FullEquationData result = new FullEquationData(count);

		for (int iii = 0; iii < result.getSize(); ++iii)
		{
			result.setEquationDataAt(tempData[iii].copy(), iii);
		}

		//If there was no operation done, flag the equation as complete.
		if (operationPerformed == false)
		{
			result.setCompletionStatus(true);
		}

		//Check the equation data to be kosher.
		result = checkFullEquationData(result);

		if (result == null)
		{
			System.out.println("Error: equationStep failure in the final check of the equation!");
		}

		//Return.
		return result;
	}

	/**
	 * Checks whether two EquationData objects can be added. In order to
	 * add like terms, both EquationData objects must have the same
	 * exact variable parts with the same exact exponents, or both must
	 * have no variable parts at all.
	 *
	 * @param inputOne The first EquationData object being checked.
	 * @param inputTwo The second EquationData object being checked.
	 * @return Returns 1 if the two EquationData objects can be added,
	 * 	   0 if they cannot be added, or -1 if there was an error.
	 * @see totalVariablesWithExps
	 */
	public static int canEquationsBeAdded(EquationData inputOne, EquationData inputTwo)
	{
		//NOTE: Assumes that the EquationData being input has already been checked
		//with one of the checker functions (e.g. checkEquationData).
		final int CAN_BE_ADDED = 1;
		final int CANNOT_BE_ADDED = 0;
		final int ERROR = -1;

		//TODO: Check data here?

		//If either input is null, error out.
		if (inputOne == null || inputTwo == null)
		{
			System.out.println("Error: canEquationsBeAdded inputOne and/or inputTwo are null!");
			return ERROR;
		}

		//If either input is an operator, error out.
		if (inputOne.isOperator() || inputTwo.isOperator())
		{
			System.out.println("Error: canEquationsBeAdded inputOne and/or inputTwo are operators!");
			return ERROR;
		}

		//If the number of variables of two EquationDatas dont match,
		//obviously we can't add them.
		if (inputOne.getSize() != inputTwo.getSize())
		{
			return CANNOT_BE_ADDED;
		}

		//If both inputs have no variables, addition will always succeed.
		if (inputOne.getSize() == 0)
		{
			return CAN_BE_ADDED;
		}

		//Just fail if 2*inputOne.numVariables is greater than the maximum value of
		//an integer. (It should be impossible anyways because there's only 26 letters
		//in the alphabet.)
		if (inputOne.getSize() > (Integer.MAX_VALUE / 2))
		{
			return CANNOT_BE_ADDED;
		}

		//Compare the counter of our accumulated variables
		//to the number of variables in our inputs. If equal,
		//return 1; otherwise, 0.
		if (totalVariablesWithExps(inputOne, inputTwo) == inputOne.getSize())
		{
			return CAN_BE_ADDED;
		}

		return CANNOT_BE_ADDED;
	}

	/**
	 * Performs addition between two EquationData objects. If the two
	 * cannot be added, or there is some kind of error, returns null.
	 *
	 * @param inputOne The first EquationData object being added.
	 * @param inputTwo The second EquationData object being added.
	 * @return Returns a new EquationData object which is the result
	 * 	   of the addition, or null if the addition failed.
	 * @see addEquationsInternal
	 */
	public static EquationData addEquations(EquationData inputOne, EquationData inputTwo)
	{
		final boolean IS_SUBTRACTION = false;

		return addEquationsInternal(inputOne, inputTwo, IS_SUBTRACTION);
	}

	/**
	 * Performs subtraction between two EquationData objects. If the two
	 * cannot be subtracted, or there is some kind of error, returns null.
	 *
	 * @param inputOne The EquationData object being subtracted from.
	 * @param inputTwo The EquationData object being subtracted.
	 * @return Returns a new EquationData object which is the result
	 * 	   of the subtraction, or null if the addition failed.
	 * @see addEquationsInternal
	 */
	public static EquationData subtractEquations(EquationData inputOne, EquationData inputTwo)
	{
		final boolean IS_SUBTRACTION = true;

		return addEquationsInternal(inputOne, inputTwo, IS_SUBTRACTION);
	}

	/**
	 * Internal function which performs both addition and subtraction,
	 * in order to reduce boilerplate code. If the parameter isSubtraction
	 * is set to true, inputTwo is subtracted from inputOne; otherwise
	 * the two are added together, and the order therefore does not matter.
	 *
	 * @param inputOne The EquationData object being subtracted from if
	 * 		   isSubtraction is true; otherwise, one of the two
	 * 		   EquationData objects being added together.
	 * @param inputOne The EquationData object being subtracted if
	 * 		   isSubtraction is true; otherwise, one of the two
	 * 		   EquationData objects being added together.
	 * @return Returns a new EquationData object which is the result
	 * 	   of the addition or subtraction, or null if the operation
	 * 	   failed.
	 * @see addEquations
	 * @see subtractEquations
	 */
	public static EquationData addEquationsInternal(EquationData inputOne, EquationData inputTwo, boolean isSubtraction)
	{
		//Fix any problems in the given EquationData objects (before they get any
		//worse).
		EquationData checkedInputOne = checkEquationData(inputOne);
		EquationData checkedInputTwo = checkEquationData(inputTwo);

		//If verification failed, fail.
		if (checkedInputOne == null || checkedInputTwo == null)
		{
			System.out.println("Error: addEquationsInternal checkedInputOne and/or checkedInputTwo failed verification!");
			return null;
		}

		//If the inputs are operators, exit and return null.
		if (checkedInputOne.isOperator() || checkedInputTwo.isOperator())
		{
			System.out.println("Error: addEquationsInternal checkedInputOne and/or checkedInputTwo are operators!");
			return null;
		}

		//If checkedInputTwo is 0, return checkedInputOne.
		if (checkedInputTwo.getNumerator() == 0)
		{
			return checkedInputOne;
		}

		//Holds the value of the object we will return.
		EquationData result;

		//If checkedInputOne is 0, return checkedInputTwo if addition, or
		//the inverse of checkedInputTwo if subtraction.
		if (checkedInputOne.getNumerator() == 0)
		{
			if (isSubtraction)
			{
				result = checkedInputTwo.copy();
				result.setNumerator(result.getNumerator() * (-1));

				return result;
			}

			else
			{
				return checkedInputTwo;
			}
		}

		//If data objects are not addable or are invalid, fail.
		if (canEquationsBeAdded(checkedInputOne, checkedInputTwo) <= 0)
		{
			return null;
		}

		//If we are going to overflow a long during the calculation, fail.
		if (Math.abs(checkedInputOne.getDenominator()) > (Long.MAX_VALUE / Math.abs(checkedInputTwo.getDenominator())))
		{
			System.out.println("Error: addEquationsInternal addition calculation will overflow a long! 1");
			return null;
		}

		if ((Math.abs(checkedInputOne.getNumerator()) > (Long.MAX_VALUE / Math.abs(checkedInputTwo.getDenominator())))
		    || (Math.abs(checkedInputTwo.getNumerator()) > (Long.MAX_VALUE / Math.abs(checkedInputOne.getDenominator()))))
		{
			System.out.println("Error: addEquationsInternal addition calculation will overflow a long! 2");
			return null;
		}

		long tempOne = checkedInputOne.getNumerator() * checkedInputTwo.getDenominator();
		long tempTwo = checkedInputTwo.getNumerator() * checkedInputOne.getDenominator();

		if (tempOne != 0 && tempTwo != 0)
		{
			if (isSubtraction)
			{
				if ((tempOne > 0 && tempTwo < 0) || (tempOne < 0 && tempTwo > 0))
				{
					if (Math.abs(tempOne) > (Long.MAX_VALUE - Math.abs(tempTwo)))
					{
						System.out.println("Error: addEquationsInternal addition calculation will overflow a long! 3");
						return null;
					}
				}
			}

			else
			{
				if ((tempOne > 0 && tempTwo > 0) || (tempOne < 0 && tempTwo < 0))
				{
					if (Math.abs(tempOne) > (Long.MAX_VALUE - Math.abs(tempTwo)))
					{
						System.out.println("Error: addEquationsInternal addition calculation will overflow a long! 4");
						return null;
					}
				}
			}
		}

		//Create the result data object we will return.
		//We copy one of the inputs so that we keep the variable
		//part of the term (without doing anything by hand).
		result = checkedInputOne.copy();

		//Here we are getting both the fractions to have the same base
		//and then adding them together (like you would do by hand).
		result.setDenominator(checkedInputOne.getDenominator() * checkedInputTwo.getDenominator());

		//If it's a subtraction, subtract the second object,
		//otherwise add it.
		if (isSubtraction)
		{
			result.setNumerator((checkedInputOne.getNumerator() * checkedInputTwo.getDenominator())
					    - (checkedInputTwo.getNumerator() * checkedInputOne.getDenominator()));
		}

		else
		{
			result.setNumerator((checkedInputOne.getNumerator() * checkedInputTwo.getDenominator())
					    + (checkedInputTwo.getNumerator() * checkedInputOne.getDenominator()));
		}

		//Stores the greatest common divisor of the new numerator and denominator.
		//we use Math.abs() for the numerator because it may be negative.
		long gcd = MathFunctions.gcd(result.getDenominator(), result.getNumerator());

		//Divides the numerator and denominator of the coefficient
		//by the greatest common denominator to simplify them.
		result.setDenominator(result.getDenominator() / gcd);
		result.setNumerator(result.getNumerator() / gcd);

		return result;
	}

	//Multiplies inputOne and inputTwo.
	public static EquationData multiplyEquations(EquationData inputOne, EquationData inputTwo)
	{
		final boolean IS_DIVISION = false;

		return multiplyEquationsInternal(inputOne, inputTwo, IS_DIVISION);
	}

	//Divides inputTwo from inputOne.
	public static EquationData divideEquations(EquationData inputOne, EquationData inputTwo)
	{
		final boolean IS_DIVISION = true;

		return multiplyEquationsInternal(inputOne, inputTwo, IS_DIVISION);
	}

	public static EquationData multiplyEquationsInternal(EquationData inputOne, EquationData inputTwo, boolean isDivision)
	{
		//Fix any problems in the given EquationData objects (before they get any
		//worse).
		EquationData checkedInputOne = checkEquationData(inputOne);
		EquationData checkedInputTwo = checkEquationData(inputTwo);

		//Stores our final result that we return.
		EquationData result;

		if (checkedInputOne == null || checkedInputTwo == null)
		{
			System.out.println("Error: multiplyEquationsInternal checkedInputOne and/or checkedInputTwo failed verification!");
			return null;
		}

		//If c or d are operators, exit and return null.
		if (checkedInputOne.isOperator() || checkedInputTwo.isOperator())
		{
			System.out.println("Error: multiplyEquationsInternal checkedInputOne and/or checkedInputTwo are operators!");
			return null;
		}

		if (isDivision)
		{
			//If we're dividing by 0, fail.
			if (checkedInputTwo.getNumerator() == 0)
			{
				System.out.println("Error: multiplyEquationsInternal division by zero!");
				return null;
			}

			//If the numerator is 0, return 0.
			if (checkedInputOne.getNumerator() == 0)
			{
				result = new EquationData(0);
				result.setNumerator(0);
				result.setDenominator(1);

				return result;
			}

			//If during the multiplication of the coefficients we end up with a number that
			//overflows a long, just fail.
			if (Math.abs(checkedInputOne.getNumerator()) > (Long.MAX_VALUE / Math.abs(checkedInputTwo.getDenominator())))
			{
				System.out.println("Error: multiplyEquationsInternal multiplication calculation will overflow a long! 1");
				return null;
			}

			if (Math.abs(checkedInputOne.getDenominator()) > (Long.MAX_VALUE / Math.abs(checkedInputTwo.getNumerator())))
			{
				System.out.println("Error: multiplyEquationsInternal multiplication calculation will overflow a long! 2");
				return null;
			}
		}

		else
		{
			//If either operand is 0, return 0.
			if (checkedInputOne.getNumerator() == 0 || checkedInputTwo.getNumerator() == 0)
			{
				result = new EquationData(0);

				result.setNumerator(0);
				result.setDenominator(1);

				return result;
			}

			//If during the multiplication of the coefficients we end up with a number that
			//overflows a long, just fail.
			if (Math.abs(checkedInputOne.getNumerator()) > (Long.MAX_VALUE / Math.abs(checkedInputTwo.getNumerator())))
			{
				System.out.println("Error: multiplyEquationsInternal multiplication calculation will overflow a long! 3");
				return null;
			}

			if (Math.abs(checkedInputOne.getDenominator()) > (Long.MAX_VALUE / Math.abs(checkedInputTwo.getDenominator())))
			{
				System.out.println("Error: multiplyEquationsInternal multiplication calculation will overflow a long! 4");
				return null;
			}
		}


		//Get the total variables so we can create arrays of that size.
		int totalVars = totalVariables(checkedInputOne, checkedInputTwo);

		char[] vars = new char[totalVars];
		long[] varExps = new long[totalVars];
		int counter = 0;
		boolean foundFlag = false;

		//For this algorithm, first we loop through the variables of checkedInputOne,
		//and if they are also present in checkedInputTwo, then just add the exponents
		//(for divison, subtract checkedInputTwo's exponent from checkedInputOne's). If
		//any are not present in b, just copy them over as they are.
		for (int iii = 0; iii < checkedInputOne.getSize(); ++iii)
		{
			for (int jjj = 0; jjj < checkedInputTwo.getSize(); ++jjj)
			{
				if (checkedInputOne.getVariableAt(iii) == checkedInputTwo.getVariableAt(jjj))
				{
					vars[counter] = checkedInputOne.getVariableAt(iii);

					if (isDivision)
					{
						varExps[counter] = checkedInputOne.getVariableExponentAt(iii) - checkedInputTwo.getVariableExponentAt(jjj);
					}

					else
					{
						varExps[counter] = checkedInputOne.getVariableExponentAt(iii) + checkedInputTwo.getVariableExponentAt(jjj);
					}

					++counter;
					foundFlag = true;
					break;
				}
			}

			if (!foundFlag)
			{
				vars[counter] = checkedInputOne.getVariableAt(iii);
				varExps[counter] = checkedInputOne.getVariableExponentAt(iii);
				++counter;
			}

			else
			{
				foundFlag = false;
			}
		}

		//Now we round up the variables in checkedInputTwo that were not also in
		//checkedInputOne.
		for (int iii = 0; iii < checkedInputTwo.getSize(); ++iii)
		{
			for (int jjj = 0; jjj < counter; ++jjj)
			{
				if (checkedInputTwo.getVariableAt(iii) == vars[jjj])
				{
					foundFlag = true;
					break;
				}
			}

			if (!foundFlag)
			{
				vars[counter] = checkedInputTwo.getVariableAt(iii);

				if (isDivision)
				{
					varExps[counter] = checkedInputTwo.getVariableExponentAt(iii) * (-1);
				}

				else
				{
					varExps[counter] = checkedInputTwo.getVariableExponentAt(iii);
				}

				++counter;
			}

			else
			{
				foundFlag = false;
			}
		}

		//Make sure that none of the variables now have an exponent of 0.
		char[] varsTwo = new char[counter];
		long[] varExpsTwo = new long[counter];
		int counterTwo = 0;

		for (int iii = 0; iii < counter; ++iii)
		{
			if (varExps[iii] != 0)
			{
				varsTwo[counterTwo] = vars[iii];
				varExpsTwo[counterTwo] = varExps[iii];
				++counterTwo;
			}
		}

		//Create our result object.
		result = new EquationData(counterTwo);

		//Copy over the variables we accumulated into the result object.
		for (int iii = 0; iii < result.getSize(); ++iii)
		{
			result.setVariableAt(varsTwo[iii], iii);
			result.setVariableExponentAt(varExpsTwo[iii], iii);
		}

		//Complete the multiplication of the coefficients.
		if (isDivision)
		{
			result.setNumerator(checkedInputOne.getNumerator() * checkedInputTwo.getDenominator());
			result.setDenominator(checkedInputOne.getDenominator() * checkedInputTwo.getNumerator());
		}

		else
		{
			result.setNumerator(checkedInputOne.getNumerator() * checkedInputTwo.getNumerator());
			result.setDenominator(checkedInputOne.getDenominator() * checkedInputTwo.getDenominator());
		}

		//Simplify the coefficient of result using the gcd, and return.
		long gcd = MathFunctions.gcd(result.getNumerator(), result.getDenominator());

		result.setNumerator(result.getNumerator() / gcd);
		result.setDenominator(result.getDenominator() / gcd);

		return result;
	}

	//Finds the total number of variables between two EquationData objects.
	public static int totalVariables(EquationData inputOne, EquationData inputTwo)
	{
		//TODO: Do we need to check the inputs here?

		//If inputOne or inputTwo are operators, fail.
		if (inputOne.isOperator() || inputTwo.isOperator())
		{
			System.out.println("Error: totalVariables inputOne and/or inputTwo are operators!");
			return -1;
		}

		//We do calculations with integers throughout related to
		//the maximum amount of possible variables
		//(e.g. inputOne.numVariables + inputTwo.numVariables), so if that total
		//is too big, just fail.
		if (inputOne.getSize() > (Integer.MAX_VALUE - inputTwo.getSize()))
		{
			System.out.println("Error: totalVariables calculation overflows a long!");
			return -1;
		}

		//Create array to hold all the unique variables.
		char[] vars = new char[inputOne.getSize() + inputTwo.getSize()];

		//A counter to track where we are in the above arrays.
		int counter = 0;

		//Flag for if we found a variable in the array or not.
		boolean foundFlag = false;

		//Now fill the unique variables arrays:
		//loop through each of the variables in inputOne
		//for each one, compare it to the accumulated variables.
		//if it isn't already accumulated, add it to the accumulated
		//variables (our local vars[] and varExps[] arrays).
		for (int iii = 0; iii < inputOne.getSize(); ++iii)
		{
			for (int jjj = 0; jjj < counter; ++jjj)
			{
				if (inputOne.getVariableAt(iii) == vars[jjj])
				{
					foundFlag = true;
					break;
				}
			}

			if (!foundFlag)
			{
				vars[counter] = inputOne.getVariableAt(iii);
				++counter;
			}

			foundFlag = false;
		}

		//Same as above, but do it for object inputTwo as well.
		for (int iii = 0; iii < inputTwo.getSize(); ++iii)
		{
			for (int jjj = 0; jjj < counter; ++jjj)
			{
				if (inputTwo.getVariableAt(iii) == vars[jjj])
				{
					foundFlag = true;
					break;
				}
			}

			if (!foundFlag)
			{
				vars[counter] = inputTwo.getVariableAt(iii);
				++counter;
			}

			foundFlag = false;
		}

		return counter;
	}

	//finds the total number of variables between two EquationData objects,
	//and treats variables with different powers as different variables.
	//For use in addition/subtraction.
	public static int totalVariablesWithExps(EquationData inputOne, EquationData inputTwo)
	{
		//TODO: Do we need to check the inputs here?

		//If inputOne or inputTwo are operators, fail.
		if (inputOne.isOperator() || inputTwo.isOperator())
		{
			System.out.println("Error: totalVariablesWithExps inputOne and/or inputTwo are operators!");
			return -1;
		}

		//We do calculations with integers throughout related to
		//the maximum amount of possible variables
		//(e.g. inputOne.numVariables + inputTwo.numVariables), so if that total
		//is too big, just fail.
		if (inputOne.getSize() > (Integer.MAX_VALUE - inputTwo.getSize()))
		{
			System.out.println("Error: totalVariables calculation overflows a long!");
			return -1;
		}

		//Create arrays to hold all the unique variables.
		char[] vars = new char[inputOne.getSize() + inputTwo.getSize()];
		long[] varExps = new long[inputOne.getSize() + inputTwo.getSize()];

		//A counter to track where we are in the above arrays.
		int counter = 0;

		boolean foundFlag = false;

		//Now fill the unique variables arrays:
		//loop through each of the variables in inputOne
		//--AND-- their exponents;
		//for each one, compare it to the accumulated variables.
		//If it isn't already accumulated, add it to the accumulated
		//variables (our local vars[] and varExps[] arrays).
		for (int iii = 0; iii < inputOne.getSize(); ++iii)
		{
			for (int jjj = 0; jjj < counter; ++jjj)
			{
				if (inputOne.getVariableAt(iii) == vars[jjj] && inputOne.getVariableExponentAt(iii) == varExps[jjj])
				{
					foundFlag = true;
					break;
				}
			}

			if (!foundFlag)
			{
				vars[counter] = inputOne.getVariableAt(iii);
				varExps[counter] = inputOne.getVariableExponentAt(iii);
				++counter;
			}

			foundFlag = false;
		}

		foundFlag = false;

		//Same as above, but do it for object inputTwo as well.
		for (int iii = 0; iii < inputTwo.getSize(); ++iii)
		{
			for (int jjj = 0; jjj < counter; ++jjj)
			{
				if (inputTwo.getVariableAt(iii) == vars[jjj] && inputTwo.getVariableExponentAt(iii) == varExps[jjj])
				{
					foundFlag = true;
					break;
				}
			}

			if (!foundFlag)
			{
				vars[counter] = inputTwo.getVariableAt(iii);
				varExps[counter] = inputTwo.getVariableExponentAt(iii);
				++counter;
			}

			foundFlag = false;
		}

		return counter;
	}

	//Checks EquationData to see if everything is valid/ok (e.g. no divide
	//by zero, no negatives in denominator, no 0th powers)
	//
	//If any problems are found, this method fixes them in a new object
	//and then returns that object as the result
	public static EquationData checkEquationData(EquationData input)
	{
		EquationData result;

		//If input is null, return null.
		if (input == null)
		{
			System.out.println("Error: checkEquationData input is null!");
			return null;
		}

		//If the input is an operator, just check that it's valid, and if not, return null.
		if (input.isOperator())
		{
			if (input.getOperator() != '*' && input.getOperator() != '/' && input.getOperator() != '+' && input.getOperator() != '-')
			{
				System.out.println("Error: checkEquationData input is an invalid operator!");
				return null;
			}

			return input;
		}

		//If the denominator is 0, return null.
		if (input.getDenominator() == 0)
		{
			System.out.println("Error: checkEquationData division by zero!");
			return null;
		}

		//If the numerator is 0, just return a mostly empty EquationData
		//with a numerator of 0 and a denominator of 1.
		if (input.getNumerator() == 0)
		{
			result = new EquationData(0);
			result.setNumerator(0);
			result.setDenominator(1);

			return result;
		}

		//Check for multiple of the same variable and fix.
		char[] vars = new char[input.getSize()];
		long[] varExps = new long[input.getSize()];
		int counter = 0;
		boolean foundFlag = false;

		for (int iii = 0; iii < input.getSize(); ++iii)
		{
			for (int jjj = 0; jjj < counter; ++jjj)
			{
				if (input.getVariableAt(iii) == vars[jjj])
				{
					varExps[jjj] = varExps[jjj] + input.getVariableExponentAt(iii);
					foundFlag = true;
					break;
				}
			}

			if (!foundFlag)
			{
				vars[counter] = input.getVariableAt(iii);
				varExps[counter] = input.getVariableExponentAt(iii);
				++counter;
			}

			else
			{
				foundFlag = false;
			}
		}

		//Check for variables raised to the 0th power and remove.
		char[] varsTwo = new char[counter];
		long[] varExpsTwo = new long[counter];
		int counterTwo = 0;

		for (int iii = 0; iii < counter; ++iii)
		{
			if (varExps[iii] != 0)
			{
				varsTwo[counterTwo] = vars[iii];
				varExpsTwo[counterTwo] = varExps[iii];
				++counterTwo;
			}
		}

		//Canonical form: reorganize variables to be in alphabetical order.
		//This is a bubble sort algorithm based off pseudocode found on the Wikipedia
		//page "Bubble sort."
		boolean swapped = false;
		char swappingCharTemp = ' ';
		long swappingLongTemp = 0;
		final int MAX_SWAPS = 500;
		int swapCount = 0;

		do
		{
			swapped = false;

			for (int iii = 1; iii < counterTwo; ++iii)
			{
				int swapResult = UtilityMethods.compareLettersAlphabetically(varsTwo[iii - 1], varsTwo[iii]);

				if (swapResult == 2)
				{
					swappingCharTemp = varsTwo[iii - 1];
					varsTwo[iii - 1] = varsTwo[iii];
					varsTwo[iii] = swappingCharTemp;

					swappingLongTemp = varExpsTwo[iii - 1];
					varExpsTwo[iii - 1] = varExpsTwo[iii];
					varExpsTwo[iii] = swappingLongTemp;

					swapped = true;
				}

				else if (swapResult < 0)
				{
					System.out.println("Error: checkEquationData failure in sort check!");
					return null;
				}
			}

			++swapCount;

		} while (swapped && (swapCount < MAX_SWAPS));

		if (swapCount >= MAX_SWAPS)
		{
			System.out.println("Error: checkEquationData sorting reached maximum allowed swaps!");
			return null;
		}

		//Now, assign the variables in varsTwo and the powers in varExpsTwo
		//to a new object in result.
		result = new EquationData(counterTwo);

		for (int iii = 0; iii < result.getSize(); ++iii)
		{
			result.setVariableAt(varsTwo[iii], iii);
			result.setVariableExponentAt(varExpsTwo[iii], iii);
		}

		//Check sign of the numerator and denominator of
		//the coefficient, and assign the fixed coefficient to
		//result.
		if (input.getNumerator() < 0 && input.getDenominator() < 0)
		{
			result.setNumerator(Math.abs(input.getNumerator()));
			result.setDenominator(Math.abs(input.getDenominator()));
		}

		else if (input.getDenominator() < 0)
		{
			result.setNumerator(input.getNumerator() * (-1));
			result.setDenominator(Math.abs(input.getDenominator()));
		}

		else
		{
			result.setNumerator(input.getNumerator());
			result.setDenominator(input.getDenominator());
		}

		//Simplify the fraction of the coefficient.
		long gcd = MathFunctions.gcd(result.getNumerator(), result.getDenominator());

		result.setNumerator(result.getNumerator() / gcd);
		result.setDenominator(result.getDenominator() / gcd);

		//Return.
		return result;
	}

	//Checks all the data in a FullEquationData object with checkEquationData. Also
	//checks for certain problems, e.g. beginning with a multiplication/divison,
	//ending with any operator.
	public static FullEquationData checkFullEquationData(FullEquationData input)
	{
		FullEquationData tempFull = new FullEquationData(input.getSize());
		FullEquationData tempFullTwo;
		EquationData tempSingle;
		int count = 0;

		if (input.getSize() <= 0)
		{
			System.out.println("Error: checkFullEquationData equation is empty!");
			return null;
		}

		//Do a basic check of all of the data for any errors like division by zero
		//or invalid operators.
		for (int iii = 0; iii < input.getSize(); ++iii)
		{
			if (input.getEquationDataAt(iii) != null)
			{
				tempSingle = checkEquationData(input.getEquationDataAt(iii));

				if (tempSingle == null)
				{
					System.out.println("Error: checkFullEquationData checkEquationData failed on data object " + iii + "!");
					return null;
				}

				tempFull.setEquationDataAt(tempSingle, count);
				++count;
			}
		}

		//Checks for two operators in a row. If found, fail.
		//(We don't support parentheses or anything yet anyways.)
		boolean lastWasOperator = false;

		for (int iii = 0; iii < count; ++iii)
		{
			if (tempFull.getEquationDataAt(iii).isOperator())
			{
				if (lastWasOperator)
				{
					System.out.println("Error: checkFullEquationData two operators in a row!");
					return null;
				}

				else
				{
					lastWasOperator = true;
				}
			}

			else
			{
				lastWasOperator = false;
			}
		}

		//Checks for leading operator. If * or /, fail. If + or -, just shift the equation left by one
		//(and multiply the first non-operator by -1 if -).
		//
		//Also, if the equation is only an operator, fail.
		if (tempFull.getEquationDataAt(0).isOperator())
		{
			if (count > 1)
			{
				if (tempFull.getEquationDataAt(0).getOperator() == '*' || tempFull.getEquationDataAt(0).getOperator() == '/')
				{
					System.out.println("Error: checkFullEquationData equation starts with * or /!");
					return null;
				}

				else if (tempFull.getEquationDataAt(0).getOperator() == '+')
				{
					for (int iii = 0; iii < count - 1; ++iii)
					{
						tempFull.setEquationDataAt(tempFull.getEquationDataAt(iii + 1).copy(), iii);
					}

					count--;
				}

				else if (tempFull.getEquationDataAt(0).getOperator() == '-')
				{
					for (int iii = 0; iii < count - 1; ++iii)
					{
						tempFull.setEquationDataAt(tempFull.getEquationDataAt(iii + 1).copy(), iii);
					}

					tempFull.getEquationDataAt(0).setNumerator(tempFull.getEquationDataAt(0).getNumerator() * (-1));

					count--;
				}
			}

			else if (count == 1)
			{
				System.out.println("Error: checkFullEquationData equation is only an operator!");
				return null;
			}
		}

		//If equation ends in operator, fail.
		if (tempFull.getEquationDataAt(count - 1).isOperator())
		{
			System.out.println("Error: checkFullEquationData equation ends in operator!");
			return null;
		}

		//Canonical form: convert any subtraction to addition with negative numbers.
		for (int iii = 0; iii < count; ++iii)
		{
			if (tempFull.getEquationDataAt(iii).isOperator() && tempFull.getEquationDataAt(iii).getOperator() == '-')
			{
				tempFull.getEquationDataAt(iii + 1).setNumerator(tempFull.getEquationDataAt(iii + 1).getNumerator() * (-1));
				tempFull.setEquationDataAt(new EquationData('+'), iii);
			}
		}

		//Canonical form: convert division to multiplication of inverse.
		long tempNumerator = 0;
		long tempDenominator = 0;

		for (int iii = 0; iii < count; ++iii)
		{
			if (tempFull.getEquationDataAt(iii).isOperator() && tempFull.getEquationDataAt(iii).getOperator() == '/')
			{
				//Copy equation data we're going to be getting the inverse of.
				tempSingle = tempFull.getEquationDataAt(iii + 1).copy();

				//Get the numerator and denominator into temp variables.
				tempNumerator = tempSingle.getNumerator();
				tempDenominator = tempSingle.getDenominator();

				//If we're going to end up with 0 in the denominator after flipping, fail.
				if (tempNumerator == 0)
				{
					System.out.println("Error: checkFullEquationData reversing numerator and denominator resulted in division by zero!");
					return null;
				}

				//Flip the copy's numerator and denominator.
				tempSingle.setNumerator(tempDenominator);
				tempSingle.setDenominator(tempNumerator);

				//Make all the variable exponents negative.
				for (int jjj = 0; jjj < tempSingle.getSize(); ++jjj)
				{
					tempSingle.setVariableExponentAt(tempSingle.getVariableExponentAt(jjj) * (-1), jjj);
				}

				//Make the operator a * instead of a /
				tempFull.setEquationDataAt(new EquationData('*'), iii);

				//Finally, set the EquationData object to be the one we flipped
				tempFull.setEquationDataAt(tempSingle, (iii + 1));

				//Clear tempSingle to hopefully conserve some RAM with the garbage collector.
				tempSingle = null;
			}
		}

		//Create result equation and return.
		FullEquationData result = new FullEquationData(count);

		for (int iii = 0; iii < result.getSize(); ++iii)
		{
			result.setEquationDataAt(tempFull.getEquationDataAt(iii).copy(), iii);
		}

		result.setCompletionStatus(input.isComplete());

		return result;
	}

	//Sorts the completed equation alphabetically, the final step before outputting.
	//Only works on a completed equation, e.g. one which has had all of the multiplication
	//and division simplified out, down to just addition.
	//
	//NOTE: for simplicity, we assume that we're only dealing with addition, because
	//checkFullEquationData should change all subtraction to addition anyways (which we
	//run immediately in this method).
	public static FullEquationData sortCompletedEquation(FullEquationData inputEquation)
	{
		//Check input equation using the standard check method.
		FullEquationData checkedInput = checkFullEquationData(inputEquation);

		//If the equation is only one EquationData long, just return it.
		if (checkedInput.getSize() == 1)
		{
			return checkedInput;
		}

		//This algorithm only works with an equation consisting of addition.
		//If there's any *, -, or / operators, fail.
		for (int iii = 0; iii < checkedInput.getSize(); ++iii)
		{
			EquationData currentData = checkedInput.getEquationDataAt(iii);
			if (currentData.isOperator() && ((currentData.getOperator() == '*') || (currentData.getOperator() == '/') ||
			    (currentData.getOperator() == '-')))
			{
				System.out.println("Error: sortCompletedEquation equation contains subtraction/multiplication/division!");
				return null;
			}
		}

		//Bubble sort algorithm using the compareEquationDataAlphabetically method to
		//decide which member of the equation should come first.
		boolean swapped = false;

		do
		{
			swapped = false;

			//NOTE: this will fail spectacularly if the equation isn't exactly formatted as
			//term - operator - term - operator - term etc. But, so will the rest of this
			//program, so no need to worry.
			for (int iii = 2; iii < checkedInput.getSize(); iii += 2)
			{
				EquationData firstData = checkedInput.getEquationDataAt(iii - 2);
				EquationData secondData = checkedInput.getEquationDataAt(iii);
				int comparisonResult = compareEquationDataAlphabetically(firstData, secondData);

				if (comparisonResult < 0)
				{
					System.out.println("Error: sortCompletedEquation comparison check resulted in error!");

					return null;
				}

				if (comparisonResult == 2)
				{
					checkedInput.setEquationDataAt(secondData.copy(), (iii - 2));
					checkedInput.setEquationDataAt(firstData.copy(), iii);

					swapped = true;
				}
			}

		} while (swapped);

		//Now just return checkedInput and we're done.
		return checkedInput;
	}

	//Compares the variable parts of two EquationDatas alphabetically.
	//If the first one comes first alphabetically, returns 1.
	//If the second one comes first alphabetically, returns 2.
	//If they're the same, returns 3.
	//If there's an error, returns -1.
	public static int compareEquationDataAlphabetically(EquationData inputOne, EquationData inputTwo)
	{
		final int FIRST_COMES_FIRST = 1;
		final int SECOND_COMES_FIRST = 2;
		final int SAME = 3;
		final int ERROR = -1;

		EquationData checkedInputOne = checkEquationData(inputOne);
		EquationData checkedInputTwo = checkEquationData(inputTwo);

		//If either EquationData object is an operator, fail.
		if (checkedInputOne.isOperator() || checkedInputTwo.isOperator())
		{
			System.out.println("Error: compareEquationDataAlphabetically one or both inputs are operators!");

			return ERROR;
		}

		//If eiher EquationData has a size of 0, that one goes first.
		//If both have a size of zero, return SAME (3).
		if (checkedInputOne.getSize() == 0)
		{
			if (checkedInputTwo.getSize() == 0)
			{
				return SAME;
			}

			return FIRST_COMES_FIRST;
		}

		else if (checkedInputTwo.getSize() == 0)
		{
			return SECOND_COMES_FIRST;
		}

		//Since checkEquationData sorts the variable part of each individual EquationData for us
		//already, all we have to do is step through and compare each variable. As soon as two variables
		//are not the same, the one that's first alphabetically is returned as coming first.
		for (int iii = 0; ((iii < checkedInputOne.getSize()) && (iii < checkedInputTwo.getSize())); ++iii)
		{
			char inputOneChar = checkedInputOne.getVariableAt(iii);
			char inputTwoChar = checkedInputTwo.getVariableAt(iii);
			int comparisonResult = UtilityMethods.compareLettersAlphabetically(inputOneChar, inputTwoChar);

			//If the comparison failed, return ERROR (-1).
			if (comparisonResult < 0)
			{
				System.out.println("Error: compareEquationDataAlphabetically comparison returned error!");

				return ERROR;
			}

			//If the two letters are not the same, return the result of the comparison of the letters.
			if (comparisonResult != 3)
			{
				return comparisonResult;
			}
		}

		//If we haven't returned anything yet, that means that all the letters compared were the same.
		//If we also have the same size of the two objects, that means that all of the variables
		//actually are the same, and we have to now compare exponents. Whichever has a lower exponent
		//first, comes first. (So, xy^2 comes before x^2y.)
		if (checkedInputOne.getSize() == checkedInputTwo.getSize())
		{
			for (int iii = 0; iii < checkedInputOne.getSize(); ++iii)
			{
				long inputOneExponent = checkedInputOne.getVariableExponentAt(iii);
				long inputTwoExponent = checkedInputTwo.getVariableExponentAt(iii);

				if (inputOneExponent < inputTwoExponent)
				{
					return FIRST_COMES_FIRST;
				}

				if (inputOneExponent > inputTwoExponent)
				{
					return SECOND_COMES_FIRST;
				}
			}

			//If they have the same variables and the same exponents to those variables,
			//return SAME (3).
			return SAME;
		}

		//If the sizes aren't the same, then we just return whichever is shorter.
		if (checkedInputOne.getSize() < checkedInputTwo.getSize())
		{
			return FIRST_COMES_FIRST;
		}

		return SECOND_COMES_FIRST;
	}

	//Prints a full equation as stored in an EquationData array.
	public static void printFullEquationData(FullEquationData input)
	{
		if (input == null)
		{
			System.out.println("Error: printFullEquationData object is null!");
			return;
		}

		boolean firstPrintedFlag = false;

		for (int iii = 0; iii < input.getSize(); ++iii)
		{
			if (input.getEquationDataAt(iii) != null)
			{
				if (firstPrintedFlag)
				{
					System.out.print(" ");
				}

				else
				{
					firstPrintedFlag = true;
				}

				printEquationData(input.getEquationDataAt(iii));
			}
		}
	}

	public static void printlnFullEquationData(FullEquationData input)
	{
		printFullEquationData(input);
		System.out.println("");
	}

	//Basically a debugging function just to print in a human readable form
	//the data in an EquationData object.
	public static void printEquationData(EquationData input)
	{
		if (input == null)
		{
			System.out.println("Error: printEquationData object is null!");
			return;
		}

		if (input.isOperator())
		{
			System.out.print("" + input.getOperator());
			return;
		}

		if (input.getNumerator() == 0)
		{
			System.out.print("0");
			return;
		}

		//Don't print a coefficient if it's just going to be 1
		//(and there's a variable part of the term).
		if ((input.getNumerator() == 1) && (input.getDenominator() == 1) && (input.getSize() > 0))
		{
			//Do nothing.
		}

		//If the coefficient is a fraction or negative, put it in parentheses.
		else
		{
			if ((input.getDenominator() != 1) || (input.getNumerator() < 0))
			{
				System.out.print("(");
			}

			System.out.print(input.getNumerator());

			if (input.getDenominator() != 1)
			{
				System.out.print("/" + input.getDenominator() + ")");
			}

			else if (input.getNumerator() < 0)
			{
				System.out.print(")");
			}
		}

		//Print each variable in the term.
		for (int iii = 0; iii < input.getSize(); ++iii)
		{
			System.out.print(input.getVariableAt(iii));

			if (input.getVariableExponentAt(iii) != 1)
			{
				System.out.print("^" + input.getVariableExponentAt(iii));
			}
		}
	}

	public static void printlnEquationData(EquationData input)
	{
		printEquationData(input);
		System.out.println("");
	}
}
